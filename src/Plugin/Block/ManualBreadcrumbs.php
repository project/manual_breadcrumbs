<?php

/**
 * @file
 * Renders breadcrumbs.
 */

namespace Drupal\manual_breadcrumbs\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;

/**
 * Create a block for manual breadcrumbs.
 *
 * @Block(
 *   id = "manual_breadcrumbs",
 *   admin_label = @Translation("Manual Breadcrumbs")
 * )
 */
class ManualBreadcrumbs extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $current_path = \Drupal::service('path.current')->getPath();
    $path = \Drupal::urlGenerator()->generateFromRoute('<front>', [], ['absolute' => TRUE]);
    $breadcrumbs[] = ['link' => $path, 'text' => t('Home')];

    $route_name = \Drupal::routeMatch()->getRouteName();
    switch ($route_name) {
      case 'default':
      case 'entity.node.canonical':
        $node = \Drupal::routeMatch()->getParameter('node');
        if (is_object($node)) {
          $breadcrumbs[] = ['link' => 'javascript:void(0)', 'text' => $node->getTitle()];
        }
      break;
    }

    $custom_breadcrumbs_obj = \Drupal::config('manual_breadcrumbs.common_settings');
    $custom_breadcrumbs_lsv = $custom_breadcrumbs_obj->get('manual_breadcrumbs');
    $custom_breadcrumbs_arr = trim($custom_breadcrumbs_lsv);
    if (!empty($custom_breadcrumbs_arr)) {
      $custom_breadcrumbs_arr = explode("\n", $custom_breadcrumbs_arr);
      if (count($custom_breadcrumbs_arr) > 0) {
        foreach ($custom_breadcrumbs_arr as $key => $value) {
          $value = trim($value);
          if (strpos($value, '|') !== false) {
            $custom_breadcrumbs_string[] = explode("|", $value);
          }
        }
        if (count($custom_breadcrumbs_string) > 0) {
          foreach ($custom_breadcrumbs_string as $key2 => $details) {
            $link_path = $details[0] ?? '';
            if ($link_path == $current_path) {
              $text = $details[1] ?? '';
              $links = $details[2] ?? '';
              $text_parse = explode('>', $text);
              $links_parse = explode('>', $links);
              break;
            }
          }
          if (!empty($text_parse) && !empty($links_parse) && count($text_parse) > 0) {
            $breadcrumbs = [];
            $breadcrumbs[] = ['link' => $path, 'text' => t('Home')];
            foreach ($text_parse as $text_key => $text_value) {
              $link_value = $links_parse[$text_key] ?? 'javascript:void';
              $breadcrumbs[] = ['link' => $link_value, 'text' => $text_value];
            }
          }
        }
      }
    }

    $menu_link_manager = \Drupal::service('plugin.manager.menu.link');

    $format_current_path = trim($current_path, "/");
    $query = \Drupal::database()->select('menu_link_content_data', 'mlcd');
    $query->fields('mlcd', ['id', 'link__uri']);
    $query->condition('mlcd.link__uri', '%' . $format_current_path . '%', 'LIKE');
    $query->condition('mlcd.menu_name', 'main');
    $menu_link_data = $query->execute()->fetchObject();
    if (is_object($menu_link_data)) {
      $menu_link_uri = $menu_link_data->link__uri;
      if (strpos($menu_link_uri, 'entity:node/') !== false) {
        $node_string = str_replace("entity:node/", "", $menu_link_uri);
        $menu_node_object = Node::load($node_string);
      }
      if (strpos($menu_link_uri, 'internal:/') !== false) {
        $route_parameters = [];
        $menu_link = $menu_link_manager->loadLinksByRoute($route_name, $route_parameters, 'main');
      }
    }

    if (isset($menu_node_object) && is_object($menu_node_object)) {
      $node_id = $menu_node_object->id();
      $menu_link = $menu_link_manager->loadLinksByRoute('entity.node.canonical', array('node' => $node_id), 'main');
    }
    if (isset($menu_link) && is_array($menu_link) && count($menu_link)) {
      $menu_link = reset($menu_link);
      if ($menu_link->getParent()) {
        $menu_link_name = $menu_link->getTitle();
        $current_menu_link = $menu_link->getUrlObject()->toString();
        $parents = $menu_link_manager->getParentIds($menu_link->getParent());
        if (count($parents) > 0) {
          array_reverse($parents);
          $parent = reset($parents);
          $parent = str_replace("menu_link_content:", "", $menu_link->getParent());
          $query = \Drupal::database()->select('menu_link_content', 'mlc');
          $query->leftJoin('menu_link_content_data', 'mlcd', 'mlcd.id = mlc.id');
          $query->fields('mlcd', ['title', 'link__uri']);
          $query->condition('mlc.uuid', $parent);
          $query->condition('mlc.bundle', 'main');
          $parent_link_data = $query->execute()->fetchObject();
          $parent_link_title = $parent_link_data->title;
          $parent_link = $parent_link_data->link__uri;
          $parent_link = Url::fromUri($parent_link)->toString();

          $breadcrumbs = [];
          $breadcrumbs[] = ['link' => $path, 'text' => t('Home')];
          $breadcrumbs[] = ['link' => $parent_link, 'text' => $parent_link_title];
          $breadcrumbs[] = ['link' => $current_menu_link, 'text' => $menu_link_name];
        }
      }
    }

    $custom_breadcrumbs_separator = $custom_breadcrumbs_obj->get('mb_separator');
    $default_separator = ' > ';
    if (!empty($custom_breadcrumbs_separator)) {
      $mb_separator_image = $custom_breadcrumbs_obj->get('mb_separator_image');
      $mb_separator_symbol = $custom_breadcrumbs_obj->get('mb_separator_symbol');
      if (!empty($mb_separator_image)) {
        $default_separator = '<img src="' . $mb_separator_image . '" alt="" />';
      }
      if (!empty($mb_separator_symbol)) {
        $default_separator = $mb_separator_symbol;
      }
    }

    $html = '';
    if (!empty($breadcrumbs)) {
      foreach($breadcrumbs as $key => $value) {
        if (($key + 1) !== count($breadcrumbs)) {
          $html .= '<a href="' . $value['link'] . '" class="link">
          <span>' . $value['text'] . '</span>' . $default_separator . '</a>';
        }
        else {
          $html .= '<span>' . $value['text'] . '</span>';
        }
      }
    }

    return [
      '#markup' => $html,
    ];
  }



}
