<?php

namespace Drupal\manual_breadcrumbs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;

/**
 * Configuration of manual configurations.
 */
class MBConfiguration extends ConfigFormBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CompletionRegister object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $connection) {
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'), $container->get('database')
   );
  }

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'manual_breadcrumbs.common_settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manual_breadcrumbs_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $instructions = "<div style='background:lightblue;padding:10px;line-height:2;'>
      Please enter values line by line <br/>
      <b> Only two formats are supported </b> <br/>
      1. path|Text <br/>
      2. path|Text|Link <br/>

      <b>Format 1</b>: path|Text <br/>
      <b>Example</b>: /link|Text1 <br/>

      <b>Format 2</b>: path|Text|Link <br/>
      <b>Example</b>: /link|Text1>Text2>Text3|/Link1>Link2 <br/>
      <b style='color:red;'>
        Note: Please do not provide link for last text. <br/>
        Only internal links can be used. <br/>
      </b>
      <b style='color:blue;'>
        By default '" . t('Home') . "' should be visible if breadcrumbs were not given.
      </b>
      <br/>
    </div>";

    $form['manual_breadcrumbs_instructions'] = [
      '#markup' => Markup::create($instructions),
    ];

    $form['manual_breadcrumbs'] = [
      '#type' => 'textarea',
      '#title' => 'Manual Breadcrumbs',
      '#description' => "Please refer above instructions.",
      '#default_value' => $config->get('manual_breadcrumbs'),
    ];

    $form['mb_separator'] = [
      '#type' => 'select',
      '#title' => 'Select Breadcrumbs Separator',
      '#options' => [
         '' => '--Select--',
        'image' => 'image',
        'symbol' => 'symbol'
      ],
      '#default_value' => $config->get('mb_separator'),
    ];
    $form['mb_separator_image'] = [
      '#type' => 'textfield',
      '#title' => 'Breadcrumbs Image Path',
      '#default_value' => $config->get('mb_separator_image'),
      '#states' => [
        'visible' => [
          [
            [':input[name="mb_separator"]' => ['value' => 'image']]
          ],
        ]
      ]
    ];
    $form['mb_separator_symbol'] = [
      '#type' => 'textfield',
      '#title' => 'Breadcrumbs Separator Symbol',
      '#default_value' => $config->get('mb_separator_symbol'),
      '#description' => t('This textfield value will not be trimmed.'),
      '#states' => [
        'visible' => [
          [
            [':input[name="mb_separator"]' => ['value' => 'symbol']]
          ],
        ]
      ]
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('manual_breadcrumbs', $form_state->getValue('manual_breadcrumbs'))
      ->set('mb_separator', $form_state->getValue('mb_separator'))
      ->set('mb_separator_image', $form_state->getValue('mb_separator_image'))
      ->set('mb_separator_symbol', $form_state->getValue('mb_separator_symbol'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
