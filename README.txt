Please make sure this module has been downloaded using composer.

How to install this module?
---------------------------
1. Install this module by drush command - drush en manual_breadcrumbs
2. Or install by "Extend" menu. (Drupal Admin Backend)

How to use this module?
-----------------------
Enable the block 'Manual Breadcrumbs' in specific region where we want to show
breadcrumbs.

How to configure this module?
-----------------------------
Follow path - /admin/config/manual/breadcrumbs

Instructions for providing breadcrumbs
--------------------------------------
Please enter values line by line
Only two formats are supported
1. path|Text
2. path|Text|Link
Format 1: path|Text
Example: /link|Text1
Format 2: path|Text|Link
Example: /link|Text1>Text2>Text3|/Link1>Link2
Note: Please do not provide link for last text.
Only internal links can be used.
By default 'Home' should be visible if breadcrumbs were not given.

New release targets
-------------------
1. Removing textarea and add pager
2. Make cache max configurable
